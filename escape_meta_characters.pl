#!/usr/bin/env perl

# set the strictures
use strict;
use warnings;

# ABSTRACT: Escape Meta Characters

=head1 NAME 

    Escape Meta Characters

=head2 DESCRIPTION

    Helper to /usr/bin/sumu

=head2 VERSION

    Automatically replaced/updated by g
    
    our $VERSION = '0.1.1';

=cut

our $VERSION = '0.1.1';

=head2 Declare Var

    Declare and chomp the variable

=cut

my $WORD_OR_PHRASE = "$ARGV[0]"; 
chomp $WORD_OR_PHRASE;

=head2 Escape metachars

    This is where the magic happens

=cut

$WORD_OR_PHRASE  =~ s!(\/|\'|\"|\*|\&|\.|\-|\~|\[|\]|\^|\$|\#|\@|\!|\`|\+|\,|\>|\/|\"|\=|\_|\%|\:|\;|\.|\<|\?|\(|\)|\{|\}|\|)!\\$1!g;

=head2 Print the Word

    So that the .sh file can pick and 
    show the result on cmdline

=cut

print "$WORD_OR_PHRASE";

