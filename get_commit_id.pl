#!/usr/bin/perl

use strict;

print get_commit_id();

=head2 Get Commit ID

    This is not git commit id

        Gets it from App.version (from 051, after bump to 050)

=cut

sub get_commit_id {

    my $SETTINGS_FILE = "./.settings";

    my $APP_VERSION_FILE = ''; 

    $APP_VERSION_FILE = `./get_val_from_name.pl APP_NAME`;
    chomp $APP_VERSION_FILE;

    if ( open (my $APP_V_FILE, "$APP_VERSION_FILE.version" ) ) {

        my $LINE = <$APP_V_FILE>;
        close $APP_V_FILE;

        my ($id, $date) = split(/\s+/, $LINE, 3);

        $id += 1;

        if ( $id <= 9 ) { $id = "00$id"; }
        elsif ( $id > 9 and $id <= 99 ) { $id = "0$id"; }
        else { $id = $id; }

        #
        return qq{$id}; 

    } else {

        return qq{Err42${APP_VERSION_FILE}};

    }


}

sub get_commit_id_old {
    #return "222";
    my @log = `git log -1`;
    my ($first, $id, $rest) = split(/\s+/, $log[4], 3);
    
    #    022 working on  #2
    #return $log[4];

    $id += 1;

    if ( $id <= 9 ) { $id = "00$id"; }
    elsif ( $id > 9 and $id <= 99 ) { $id = "0$id"; }
    else { $id = $id; }
    #
    return qq{$id}; 
}

1;