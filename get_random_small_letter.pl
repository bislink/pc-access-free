#!/usr/bin/env perl

use strict; 
use warnings;

my $rand_small_letter = '';

my $start = ord('a');  # ASCII value of 'a'
my $end = ord('z');    # ASCII value of 'z'

$rand_small_letter = chr(int(rand($end - $start + 1)) + $start);

print $rand_small_letter;
