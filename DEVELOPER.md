# PC Access Free or sPanel For Windows

This app is public on GitHub and GitLab

## GitHub

git@gitlab.com:bislink/pc-access-free.git

## GitLab

git@gitlab.com:bislink/pc-access-free.git

Not working at the moment. 

## Git 

Fri Jun 14 13:44:04 CDT 2024

PS C:\inetpub\wwwroot\pc-access-free> git log -1
commit 1db78c2218c6b8e25944ce430e5e04f349246f25 (HEAD -> master, tag: 0.8.8, origin/master, origin/HEAD)
Author: Bislinks Dotcom <github@bislinks.com>
Date:   Mon Apr 18 10:17:48 2022 -0500

    088 See #10

### Remote 

PS C:\inetpub\wwwroot\pc-access-free> git remote show origin
\* remote origin
  Fetch URL: git@gitlab.com:bislink/pc-access-free.git
  Push  URL: git@gitlab.com:bislink/pc-access-free.git
  HEAD branch: master
  Remote branch:
    master tracked
  Local branch configured for 'git pull':
    master merges with remote master
  Local ref configured for 'git push':
    master pushes to master (up to date)


## Windows Version Settings, Best Practices, To Dos.

### Do not use WSL for git; Use Windows Commandline or Powershell (as administrator)

## Hardware

Windows 11, HP Laptop, 32 GB RAM, 1 TB SSD

## Database Settings 

describe user;

id	bigint	NO	PRI		auto_increment
username	varchar(49)	YES			
password	varchar(99)	YES			
email	varchar(99)	YES			
date_modified	timestamp	NO		CURRENT_TIMESTAMP	DEFAULT_GENERATED on update CURRENT_TIMESTAMP
date_registered	datetime	NO		CURRENT_TIMESTAMP	DEFAULT_GENERATED
firstname	varchar(99)	YES			
lastname	varchar(99)	YES			
city	varchar(99)	YES			
state	varchar(99)	YES			
country	varchar(99)	YES			

## IDE 

use codium

## About

Originally, 'PC Access Free'; Will be renamed to 'sPanel For Windows' or something similar.


