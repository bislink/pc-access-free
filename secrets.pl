#!/usr/bin/perl

use strict;
use warnings;

# ABSTRACT: Generate SECRETS for a Mojo App

our $VERSION = '1.2.8';

=head1 NAME 

    Generate SECRETS for a Mojo App

=head2 DESCRIPTION

    Generate secrets for a Mojo App

    Get next port number

    Update ~/00_LAST_PORT with the latest port used 

=head2 VERSION

    Automatically updated by g
    
    our $VERSION = '1.2.8'
    
=head2 Get Current User

    From \${USER}

=cut

my $CURRENT_USER = `echo \${USER}`;
chomp $CURRENT_USER;

=head2 User Home Directory

    Determine the correct home directory path by checking if user is root or not

=cut

my $USER_HOME_DIR;
if ( $CURRENT_USER eq 'root' ) {
    $USER_HOME_DIR = "/root/";
} else {
    $USER_HOME_DIR = "/home/$CURRENT_USER";
}

=head2 Random Small Letter

    Usage 

        my $rand_small_letter = `./get_random_small_letter.pl`;  

    Should add this file to ~/m/mojoapp      

=cut

my $rand_small_letter = `./get_random_small_letter.pl`;
chomp $rand_small_letter;


=head2 Create Secrets using apg

    Should add this feature/block to ~/m/mojoapp

=cut


my $SECRETS;
my @CONT = `apg -n 19 -m 40`;
#
foreach my $line (@CONT) {
    # replace metachars with a random small letter
    $line =~ s!(\:|\[|\]\!|\.|\*|\(|\)|\&|\||\#|\=|\'|\"|\@|\<|\>|\/|\?|\,|\.|\~|\$|\-|\^|\_)!$rand_small_letter!g;
    # convert all caps to small letters
    $line = lc($line);
    # out
    $SECRETS .= qq{  - $line};
}


=head2 Get Next Port number 

    from default (the SuMu standard) file, ~/00_LAST_PORT, in user's main/home dir.

    Create one if one does not exist

    Starting port number for apps on hp laptop is 10099; Otherwise, 43210

=cut

my $NEXT_PORT;
if ( open ( my $PORT_FILE, "<", "$USER_HOME_DIR/00_LAST_PORT.txt" ) ) {
    #
    my $line = <$PORT_FILE>;
    close $PORT_FILE;

    $NEXT_PORT = $line + 1;
    
    # Also, update the port file with the latest port number 
    if ( open ( my $PORT_FILE, ">", "$USER_HOME_DIR/00_LAST_PORT.txt" ) ) {
        print $PORT_FILE "$NEXT_PORT";
        close $PORT_FILE;
    }

} else {
    # create it the pure perl way (to be compatible with windows)
    if ( open ( my $PORT_FILE, ">", "$USER_HOME_DIR/00_LAST_PORT.txt" ) ) {
        print $PORT_FILE "43210";
        close $PORT_FILE;
    }
}

=head2 USAGE 
   
    $0 APP_YAML_NAME.yml APP_NAME APP_VERSION

=head2 Arguments to $0

    First argument is the yaml file and should contain the file extension

    Seconf argument is app name

    Third argument is version (default is 0.0.0)

=head3 Declare Vars

    my $APP_YAML_FILE = "$ARGV[0]";

    my $APP_NAME = "$ARGV[1]";

    my $APP_VERSION = "$ARGV[2]";

=cut

my $APP_YAML_FILE = "$ARGV[0]" || "app${NEXT_PORT}.yml";

my $APP_NAME = "$ARGV[1]" || "App${NEXT_PORT}";

my $APP_VERSION = "$ARGV[2]" || "$VERSION" || '0.0.0';


=head2 Subroutine 'append to yaml file'

    This is where a lot of magic happens

    Usage:

        &append_to_yaml_file("$APP_YAML_FILE", "$APP_NAME", "$APP_VERSION");

=cut

sub append_to_yaml_file {

    my $file_name = shift;

    my $out;

    if (-f "$file_name") {
        #
        if ( open my $FILE, ">>", "$file_name" ) {
            print $FILE qq{$SECRETS};
            close $FILE;
        } else {
            print qq{Err#14\n};
        }
        #
    } else {

        print "File '$file_name' does not exist\n";

        print "Creating new one with secrets and thisapp elements\n";

        if ( open my $FILE, ">", "$file_name" ) {
            
            # prepare OUT
            my $OUT = '';
            #
            $OUT .= qq{---\nsecrets:\n};
            #
            $OUT .= qq{$SECRETS};
            #
            $OUT .= qq{hypnotoad:
  listen:
    -  "http://*:$NEXT_PORT"
  workers: 10
  proxy: 1
};
            #
            $OUT .= qq{thisapp:
  name: "$APP_NAME"
  description: "$APP_NAME "
  keywords: "$APP_NAME, hypnotoad, perl, shell, mojolicious, git, linux, unix, server, panel"
dev: 1
version: '$APP_VERSION'
};
            #
            print $FILE qq{$OUT};
            close $FILE;
            #
        } else {
            print qq{Err#40\n};
        }

    }
}

&append_to_yaml_file("$APP_YAML_FILE", "$APP_NAME", "$APP_VERSION");


1;
