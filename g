#!/bin/bash

: <<'END_OF_DOCS'

=head1 NAME

g

    g for git and more 

=head2 DESCRIPTION

    g for git

=head2 Default Location value for DIR

    DIR= /usr/share/sumu OR 
        /home/USER/s/sumu OR 
        /home/USER/s/usr-bin-sumu
    
    Default location of shared directory "sumu" 
    # under /usr/share
    # owned by current user

=cut

END_OF_DOCS


DIR=""

CURRENT_USER="${USER}"

if [ "${CURRENT_USER}" = "root" ]
    then
        # 
        DIR="/usr/share/sumu"
    elif [ "${CURRENT_USER}" = "ns21u2204" ]
        then
            # # needed for/as this g, although located on windows, is run from/on WSL
            DIR="/mnt/c/inetpub/wwwroot/pc-access-free" 
    else 
        
        DIR="."
fi

#colors using tput
source "$DIR/colors.sh" || "$pwd/colors.sh"

: <<'END_OF_DOCS'

=head2 VERSION, automatically updated by g

    Except when this file is being/manually edited by developer.

    VERSION='0.2.4'

=cut

END_OF_DOCS

# automatically updated by g: except when this file is being/manually edited by developer.
VERSION='0.2.4'


: <<'END_OF_DOCS'

=head2 Dates 

=cut

END_OF_DOCS

# Date 1 # Used in git commits, etc.
DATE=$(date '+%Y%m%d%H%M%S%Z');

# Date 2 # Used in logs, etc.
DATE02=$(date '+%Y-%m-%d %H:%M:%S %Z');

# Y M D H
DATE_YEAR=$(date '+%Y');
DATE_MONTH=$(date '+%m');
DATE_DAY=$(date '+%d');
DATE_HOUR=$(date '+%H');

# YEARLY
DATE_FOR_GIT_LOG_YEARLY=$(date '+%Y');
DIR_FOR_GIT_LOG_YEARLY="${DATE_YEAR}";

# MONTHLY
DATE_FOR_GIT_LOG_MONTHLY=$(date '+%Y-%m');
DIR_FOR_GIT_LOG_MONTHLY="${DATE_YEAR}/${DATE_MONTH}";

# DAILY
DATE_FOR_GIT_LOG_DAILY=$(date '+%Y-%m-%d');
DIR_FOR_GIT_LOG_DAILY="${DATE_YEAR}/${DATE_MONTH}/${DATE_DAY}";

#HOURLY
DATE_FOR_GIT_LOG_HOURLY=$(date '+%Y-%m-%d-%H');
DIR_FOR_GIT_LOG_HOURLY="${DATE_YEAR}/${DATE_MONTH}/${DATE_DAY}/${DATE_HOUR}";

# yyyy yyyy/mm yyyy/mm/dd yyyy/mm//dd/hh directories
LOG_DIRS_YMDH=("${DATE_YEAR}" "${DATE_YEAR}/${DATE_MONTH}" "${DATE_YEAR}/${DATE_MONTH}/${DATE_DAY}" "${DATE_YEAR}/${DATE_MONTH}/${DATE_DAY}/${DATE_HOUR}");

# Create ./log/yyyy/mm/dd/hh directories if do not exist
for DIR_IN_LOG in "${LOG_DIRS_YMDH[@]}"
do
    if [ -d "./log/${DIR_IN_LOG}" ]

        then

            echo "$DIR_IN_LOG already exists"

    else

        text_wrap_cyan "Creating ${DIR_IN_LOG}"

        mkdir "./log/${DIR_IN_LOG}"

        # Automatically add created date dir under ./log to git: This adds freshly brewed .gitlog file to git automatically
        git add "./log/${DIR_IN_LOG}"

    fi

done

# NNN+1
NEXT_COMMIT_ID=`./get_commit_id.pl`;

# get Next version
NEXT_VERSION=`./get_next_version_number.pl`;

# Get NNN
CURRENT_COMMIT_ID=`./get_current_commit_id.pl`;

# Get/convert NNN to N.N.N
CURRENT_VERSION=`./get_current_version_number.pl`;

COMMIT_MESSAGE=;

YAMLNAME=`./get_val_from_name.pl YAML_NAME`;

APP_NAME=`./get_val_from_name.pl APP_NAME`;

VERSION_FILE="./$APP_NAME.version";


#
VISITS_FILE="./$APP_NAME.visits";

# create above two files if they do not exist
if [ -f "./$VISITS_FILE" ]
then
    TOTAL=`cat $VISITS_FILE`;
else
    echo "Visits File does not exist. Creating one";
    touch "./$VISITS_FILE";
fi

echo ""


if [ "$1" = '' ]; then

    # do not commit
        # just show status/log/etc.

    COMMIT_MESSAGE=$DATE;

    echo "Showing last commit from log"
    git log --oneline -3

    echo "Show Status"
    git status

else
    # if $1 is not empty, commit

    COMMIT_MESSAGE="$1";

    # take care of metachars in the commit message
    PRUNED_COMMIT_MESSAGE=`./prune_commit_message.pl "${COMMIT_MESSAGE}"`;

    # update service-worker.js
    if [ -f "./public/assets/service-worker.js" ]
    then
        sed -i "s/$CURRENT_COMMIT_ID/$NEXT_COMMIT_ID/" public/assets/service-worker.js;
    else
        echo "File service-worker not found"
    fi

    echo ""

    # change version in .yml
    if [ -f "./$YAMLNAME" ]
    then
        sed -i "s/version: '$CURRENT_COMMIT_ID'/version: '$NEXT_COMMIT_ID'/" ./$YAMLNAME;
    else
        echo "File .yml not found"
    fi

    # Replace --version-- in dist.ini
    if [ -f "./dist.ini" ]
    then
        sed -i "s/version = $CURRENT_VERSION/version = $NEXT_VERSION/" ./dist.ini;
    else
        echo "File dist.ini not found"
        echo ""
    fi

    # Update 'our VERSION' 

    sed -i "s/our \$VERSION = '$CURRENT_VERSION'/our \$VERSION = '$NEXT_VERSION'/" ./lib/*.pm;

    sed -i "s/our \$VERSION = '$CURRENT_VERSION'/our \$VERSION = '$NEXT_VERSION'/" ./*.cgi;

    # update in r
    if [ -f "./r" ]
    then
        sed -i "s/VERSION='$CURRENT_VERSION'/VERSION='$NEXT_VERSION'/" ./r;
    else
        echo "File ./r not found"
        echo ""
    fi

    # update in h
    if [ -f "./h" ]
    then
        sed -i "s/VERSION='$CURRENT_VERSION'/VERSION='$NEXT_VERSION'/" ./h;
    else
        echo "File ./h not found"
        echo ""
    fi

    # update in secrets.pl
    if [ -f "./secrets.pl" ]
    then
        sed -i "s/our \$VERSION = '$CURRENT_VERSION'/our \$VERSION = '$NEXT_VERSION'/" ./secrets.pl;
    else
        echo "File ./secrets.pl not found"
        echo ""
    fi

    # Do Latest Update 
    FILES2DO_LATESTUPDATE_IN="README.md"
    #
    for FILE in ${FILES2DO_LATESTUPDATE_IN[@]}
    do
        if [ -f "./$FILE" ]
        then
            sed -i "s/\#\# Latest Update/\#\# Latest Update\n\nVersion and commit message\n\n\#\#\# ${DATE02} (${NEXT_COMMIT_ID}) \n\n\tVersion: '${NEXT_VERSION}' \n\n\t\t${PRUNED_COMMIT_MESSAGE}\n\nPowered By g\n/" ./$FILE
            
            if [ "$!" ]; then echo "254 $!"; fi
            
        else
            echo "File ./$FILE not found"
        fi
    done

    # write to CHANGELOG
    if [ -f "./CHANGELOG" ]
    then
        sed -i "s/## Latest Update/## Latest Update\n\n### ${DATE02} (${NEXT_COMMIT_ID}) \n\n  -  Version: '${NEXT_VERSION}' \n  -  ${COMMIT_MESSAGE}\n  -  Powered By g\n/" ./CHANGELOG
    else
        echo "File CHANGELOG not found"
    fi

    if [ "$!" ]; then echo "266 $!"; fi

    echo "/$CURRENT_COMMIT_ID/$NEXT_COMMIT_ID/";

    if [ -f "$VERSION_FILE" ]
    then
        # Create / Write 
        echo "$NEXT_COMMIT_ID|$DATE" > $VERSION_FILE;
        #
        echo "Latest version number from the actual 'App.version' file ";
        cat $VERSION_FILE;
        #
    else
        echo "Version file does not exist. Creating one."
        touch "$VERSION_FILE";
    fi

    # Update version in this file
    sed -i "s/VERSION='$CURRENT_VERSION'/VERSION='$NEXT_VERSION'/" ./g

    # get full commit id
    FULL_COMMIT_ID=`git rev-parse HEAD`

# Write all my commits to my own YEARLY/MONTHLY/DAILY/HOURLY .gitlog files 

# HOURLY : .gitlog files

    # DIR_FOR_GIT_LOG_HOURLY = yyyy/mm/dd/hh
    echo -e "BEGIN $NEXT_COMMIT_ID\n$DATE02\n${FULL_COMMIT_ID}\n${COMMIT_MESSAGE}\nEND $NEXT_COMMIT_ID\n" >> "./log/${DIR_FOR_GIT_LOG_HOURLY}/$NEXT_COMMIT_ID.${DATE_FOR_GIT_LOG_HOURLY}.gitlog"
    
    # add just created file to git 
    git add "./log/${DIR_FOR_GIT_LOG_HOURLY}/$NEXT_COMMIT_ID.${DATE_FOR_GIT_LOG_HOURLY}.gitlog"

# DB

    # Add created filename to an HOURLY file : sort of maintan a db/record of log files in one file for easy access later 
    echo "$NEXT_COMMIT_ID|log/${DIR_FOR_GIT_LOG_HOURLY}/$NEXT_COMMIT_ID.${DATE_FOR_GIT_LOG_HOURLY}.gitlog|${FULL_COMMIT_ID}|$DATE02" >> "./log/${DIR_FOR_GIT_LOG_HOURLY}/gitlog.hourly"

    # and, add gitlog.files to git 
    git add "./log/${DIR_FOR_GIT_LOG_HOURLY}/gitlog.hourly"

# MONTHLY 

    # No Files created; Only add HOURLY filenames to MONTHLY DB

# DB

    # Add created HOURLY filenames to a MONTHLY file 
    echo "$NEXT_COMMIT_ID|log/${DIR_FOR_GIT_LOG_MONTHLY}/$NEXT_COMMIT_ID.${DATE_FOR_GIT_LOG_MONTHLY}.gitlog|${FULL_COMMIT_ID}|$DATE02" >> "./log/${DIR_FOR_GIT_LOG_MONTHLY}/gitlog.monthly"

    # and, add gitlog.monthly file to git 
    git add "./log/${DIR_FOR_GIT_LOG_MONTHLY}/gitlog.monthly"


    #
    echo "Committing Message"
    git commit -am "$NEXT_COMMIT_ID $COMMIT_MESSAGE $(date '+%Y%m%d%H%M%S') $CURRENT_COMMIT_ID";

    if [ "$!" ]; then echo "324 $!"; fi

    # c
    # copy/scp to 225 (local rl9 server) after commit

    if [ -f ./c ]; then
        echo "Copy/Update files to another/test server via SCP or RSYNC"
        ./c
    else
        echo "./c not found";
        echo ""
    fi

    # r
    # also push to repo automatically.
    echo "Push/update Git Lab Repo"

    if [ -f ./r ]; then
        echo "Uploading to remote/git"
        ./r
    else
        echo "./r not found";
        echo ""
    fi


fi

echo "";

#echo "Latest Version number from $YAMLNAME ";
#grep -r 'version' $YAMLNAME;
#echo "";

#echo "Latest Version number from service-worker.js ";
#grep -r 'version' ./public/assets/service-worker.js;
#echo "";

#echo "Total Visits: (From $VISITS_FILE)";
#echo "$TOTAL";
#echo "";

echo "Total Commits";
git rev-list --count --all;
echo "";


