package Filedb;

=head1 NAME

	File database

=head2 DESCRIPTION

	File database

=head2 Bless

	sub new { bless {}, shift }

=cut

sub new { bless {}, shift }

=head2 Sub Check Credentials

    Check Credentials

        against a file database with items separated by a pipe sign 

            one credential per file 
=cut

sub check_credentials {

	# Mojo Lite Magick
	my $self = shift;
	
	# IN
    my %i = (
        user => '',
        pass => '',
		md5_pass => '',
		main_dir => 'C:/inetpub/sumu',
        creds_dir => 'C:/inetpub/sumu/pcaf/creds',
		data_dir => 'C:/inetpub/sumu/pcaf/data',
		@_,
    );

    # USER stuff ( internal to this subroutine )
    my %u;
    
    # OUT
    my %o;

	if ( $i{user} ne '' and $i{pass} ne '' and $i{md5_pass} ne '' )
	{
		if (open (my $USER_PASS_FILE, "$i{creds_dir}/$i{user}.user") )
		{
			while ( my $line = <$USER_PASS_FILE> ) {
                chomp $line;

                ( $u{'name'}, $u{'pass'}, $u{'dir'}, $u{'email'}, $u{the_rest} ) = split(/\|/, $line);

                ( $u{'start_date'}, $u{'end_date'}, $u{'signup_ip'}, $u{'membership_level'}, $u{'membership_status'} ) = split(/\|/, $u{the_rest});

            }

            # Populate %o
            for ("name", "pass", "dir", "email", "start_date", "end_date", "signup_ip", "membership_level", "membership_status") {
                $o{$_} = $u{$_};
            }

			# md5 comparison of pass

			if ( $i{user} eq $u{name} and $u{pass} eq $i{md5_pass} )
			{
				$o{creds_status} = '1';
			}
			else
			{
				$o{creds_status} = '0';
			}

            close $USER_PASS_FILE;

		}
		else
		{
			$o{error} = qq{<div class="alert alert-warning">$i{user} not found on this server</div>};
		}
	}
	else
	{
		$o{error} = qq{<div class="alert alert-warning">Username/Password cannot be empty</div>};
	}

    #return ($o{credentials_check}, $o{error});

    return %o;

}

# end Sub Check Credentials

=head2 Read Filedb

=cut

sub read_filedb (%i) {

	my %in = _in_();

	#

}

# end Read Filedb 


=head2 Write Filedb

=cut

sub write_filedb (%i) {

}

# end Write Filedb 

=head2 Internal: settings

	for all subrouties in this module

=cut

sub _in_ {

    my %in = (
        user => '',
        pass => '',
		main_dir => 'C:/inetpub/sumu',
        creds_dir => 'C:/inetpub/sumu/pcaf/creds',
		data_dir => 'C:/inetpub/sumu/pcaf/data',
		@_,
    );

	return %in;

}





1;

