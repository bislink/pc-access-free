#!/usr/bin/env perl

use strict;
use warnings;

our @ARGV;

if ( $ARGV[0] eq 'add' ) {
    #
    print _add_numbers( num1 => "$ARGV[1]", num2 => "$ARGV[2]" );
    #
} elsif ( $ARGV[0] eq 'subtract' ) {
    #
    print _subtract_numbers( num1 => "$ARGV[1]", num2 => "$ARGV[2]" );
    #
} else {
    print "error with numbers";
}

sub _add_numbers {
    #
    my %in = (
        num1 => 10,
        num2 => 20,
        @_,
    );

    #
    return $in{num1} + $in{num2};
}


sub _subtract_numbers {
    #
    my %in = (
        num1 => 100,
        num2 => 20,
        @_,
    );

    #
    return $in{num1} - $in{num2};
}


1;

__DATA__
