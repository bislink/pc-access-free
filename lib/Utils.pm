package Utils;

# ABSTRACT: Utilis - Perl Snippets, used everywhere

=head2 Exporter

=cut

require Exporter;
our @ISA = qw(Exporter);  # inherit all of Exporter's methods
our @EXPORT_OK = qw(cleanse get_cwd md5_digest );

=head2 Package Utils

  Utilis
  
    Perl Snippets, used everywhere

=head2 Bless, New

  sub new { bless {}, shift }

=cut

sub new { bless {}, shift }


=head3 Get Cwd
  Get Current Working Directory
  Used in get_cwd down below
    which in turn is used in admin.cgi via 'use Utils'
=cut

use Cwd qw();
my $cwd = Cwd::abs_path();
$cwd =~ s!\\!\/!g;


=head3 Cleanse A String

  remove metacharacters from a given string and return the cleansed string 

=cut

sub cleanse {
  my $self = shift;
  my %in = (
    string => "",
    @_,
  );
  if ($in{string} ne '') {
    $in{string} =~ s!(\~|\!\@|\$|\%|\^|\&|\*|\(|\)|\:|\<|\>|\,|\.|\`|\/)!!g;
  } else {
    $in{string} = "NOSTRING";
  }
  return $in{string};
}
# end cleanse


=head2 Return Current Working Directory

	This Sub just returns current working directory

=cut

sub get_cwd {
  
  my $self = shift;
  
  return $cwd;

}
# end get cwd


=head2 Get MD5

	my $md5pass = $self->utils->get_md5("PASS", "TYPE");

=cut

sub get_md5 {

	my $self = shift;

  use Digest::MD5 qw(md5 md5_hex md5_base64);

  my %i = (
    pass => '',
    type => '',
    @_,
  );
  
  my $out = '';

	if ( $i{pass}  ne '' and $i{type} ne '' ) {

		if ( $i{type} eq 'md5' ) { $out = md5("$i{pass}"); }
		elsif ( $i{type} eq 'md5_hex' ) { $out = md5_hex("$i{pass}"); }
		elsif ( $i{type} eq 'md5_base64' ) { $out = md5_base64("$i{pass}"); }
    elsif ( $i{type} eq 'sumu_crypto' ) { $out = qq{FutureUseSumuCrypt}; }
		else { $out = qq{UnknownError}; }

	} else {

    $out = qq{NoParams91};

  }

	return $out;

}
# end 





1;
