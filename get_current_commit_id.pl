#!/usr/bin/perl

use strict;
use warnings;

=head1 NAME

Get Current Commit ID

=head2 DESCRIPTION

Get Current commit Id

=head2 Get Current User and Shared DIR path

GET CURRENT USER and Shared Directory path

=cut

my $CURRENT_USER="$ENV{USER}";
    chomp $CURRENT_USER;

my $DIR = '';

if ( $CURRENT_USER eq 'root' ) { $DIR = "/usr/share/sumu"; }
elsif ( $CURRENT_USER eq 'ns21u2204' ) { $DIR = "/mnt/c/inetpub/wwwroot/pc-access-free"; }
else { $DIR = "."; }

=head2 Sub Get Current Commit ID

    Get latest Custom ID from `git log -1`

    IMPORTANT: This is NOT commit ID of git

=cut

print get_current_commit_id();

sub get_current_commit_id {

    my $SETTINGS_FILE = "./.settings";

    my $APP_VERSION_FILE = ''; 

    $APP_VERSION_FILE = `./get_val_from_name.pl APP_NAME`;
    chomp $APP_VERSION_FILE;

    if ( open (my $APP_V_FILE, "$APP_VERSION_FILE.version" ) ) {

        my $LINE = <$APP_V_FILE>;
        close $APP_V_FILE;

        my ($id, $date) = split(/\|+/, $LINE, 3);

        #
        return qq{$id}; 

    } else {

        return qq{Err60};

    } 
}

1;

__DATA__


