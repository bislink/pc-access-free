#!/bin/bash

: <<'END_OF_DOCS'

=head1 NAME

    Test Run Parts Of g

=head2 DESCRIPTION

    Test Run Parts Of g

        Does not create or write to files; Just shows them

=head2 Run .pl Files

    CommitID
    
    NextVersion

    CurrentCommitID

    CurrentVersionNumber

    YamlName

    AppName

=head2 Show 

    Show APP_NAME, YAML_NAME and some files 
    
        Cat ./settings


=cut

END_OF_DOCS

echo "Get Next Commit ID: " 
./get_commit_id.pl
echo ""

echo "Get Next Version Number: "
./get_next_version_number.pl
echo ""

echo "Get Current Commit ID: "
./get_current_commit_id.pl
echo ""

echo "Get Current Version Number: "
./get_current_version_number.pl
echo ""

echo "Get Yaml Name: "
./get_val_from_name.pl YAML_NAME
echo ""

echo "Get App Name: "
./get_val_from_name.pl APP_NAME
echo ""

APP_NAME=`./get_val_from_name.pl APP_NAME`
YAML_NAME=`./get_val_from_name.pl YAML_NAME`

echo "Show APP_NAME.version"
cat ./${APP_NAME}.version
echo ""

echo "Show Yaml Name"
echo $YAML_NAME
echo ""

echo "Cat .settings"
cat ./.settings
echo ""

