# colors used throughout the app

BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
LIME_YELLOW=$(tput setaf 190)
POWDER_BLUE=$(tput setaf 153)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
BRIGHT=$(tput bold)
NORMAL=$(tput sgr0)
BLINK=$(tput blink)
REVERSE=$(tput smso)
UNDERLINE=$(tput smul)

# red
function red_wrap_equals() {
    printf "%s =   =   =   =   =   =   =   =   =   =   =   =   =   =  %s \\n" "${RED}" "${NORMAL}"
}

function red_wrap_hyphens() {
    printf "%s -   -   -   -   -   -   -   -   -   -   -   -   -   -  %s \\n" "${RED}" "${NORMAL}"
}

# blue 

function blue_wrap_equals() {
    printf "%s =   =   =   =   =   =   =   =   =   =   =   =   =   =  %s \\n" "${BLUE}" "${NORMAL}"
}

function blue_wrap_hyphens() {
    printf "%s -   -   -   -   -   -   -   -   -   -   -   -   -   -  %s \\n" "${BLUE}" "${NORMAL}"
}

# yellow 

function yellow_wrap_equals() {
    printf "%s =   =   =   =   =   =   =   =   =   =   =   =   =   =  %s \\n" "${YELLOW}" "${NORMAL}"
}

function yellow_wrap_hyphens() {
    printf "%s -   -   -   -   -   -   -   -   -   -   -   -   -   -  %s \\n" "${YELLOW}" "${NORMAL}"
}

# green 

function green_wrap_equals() {
    printf "%s =   =   =   =   =   =   =   =   =   =   =   =   =   =  %s \\n" "${GREEN}" "${NORMAL}"
}

function green_wrap_hyphens() {
    printf "%s -   -   -   -   -   -   -   -   -   -   -   -   -   -  %s \\n" "${GREEN}" "${NORMAL}"
}

# cyan 

function cyan_wrap_equals() {
    printf "%s =   =   =   =   =   =   =   =   =   =   =   =   =   =  %s \\n" "${CYAN}" "${NORMAL}"
}

function cyan_wrap_hyphens() {
    printf "%s -   -   -   -   -   -   -   -   -   -   -   -   -   -  %s \\n" "${CYAN}" "${NORMAL}"
}

# magenta 

function magenta_wrap_equals() {
    printf "%s =   =   =   =   =   =   =   =   =   =   =   =   =   =  %s \\n" "${MAGENTA}" "${NORMAL}"
}

function magenta_wrap_hyphens() {
    printf "%s -   -   -   -   -   -   -   -   -   -   -   -   -   -  %s \\n" "${MAGENTA}" "${NORMAL}"
}

# lime yellow

function limeyellow_wrap_equals() {
    printf "%s =   =   =   =   =   =   =   =   =   =   =   =   =   =  %s \\n" "${LIME_YELLOW}" "${NORMAL}"
}

function limeyellow_wrap_hyphens() {
    printf "%s -   -   -   -   -   -   -   -   -   -   -   -   -   -  %s \\n" "${LIME_YELLOW}" "${NORMAL}"
}

#  powder blue 

function powderblue_wrap_equals() {
    printf "%s =   =   =   =   =   =   =   =   =   =   =   =   =   =  %s \\n" "${POWDER_BLUE}" "${NORMAL}"
}

function powderblue_wrap_hyphens() {
    printf "%s -   -   -   -   -   -   -   -   -   -   -   -   -   -  %s \\n" "${POWDER_BLUE}" "${NORMAL}"
}

# text wrap red

function text_wrap_red() {
    if [ "$1" != '' ]; 
        then 
            printf "%s %s %s \\n" "${RED}" "$1" "${NORMAL}" 
        else 
            printf "%s %s %s \\n" "${RED}" "NoTextGiven ToBeWrapped InRed \\nUSAGE: sumu text_wrap_COLOR TEXT" "${NORMAL}" 
    fi
}

# text wrap blue

function text_wrap_blue() {
    if [ "$1" != '' ]; 
        then 
            printf "%s %s %s \\n" "${BLUE}" "$1" "${NORMAL}" 
        else 
            printf "%s %s %s \\n" "${BLUE}" "NoTextGiven ToBeWrapped InRed \\nUSAGE: sumu text_wrap_COLOR TEXT" "${NORMAL}" 
    fi
}

# text wrap yellow

function text_wrap_yellow() {
    if [ "$1" != '' ]; 
        then 
            printf "%s %s %s \\n" "${YELLOW}" "$1" "${NORMAL}" 
        else 
            printf "%s %s %s \\n" "${YELLOW}" "NoTextGiven ToBeWrapped InRed \\nUSAGE: sumu text_wrap_COLOR TEXT" "${NORMAL}" 
    fi
}

# text wrap green

function text_wrap_green() {
    if [ "$1" != '' ]; 
        then 
            printf "%s %s %s \\n" "${GREEN}" "$1" "${NORMAL}" 
        else 
            printf "%s %s %s \\n" "${GREEN}" "NoTextGiven ToBeWrapped InRed \\nUSAGE: sumu text_wrap_COLOR TEXT" "${NORMAL}" 
    fi
}

# text wrap cyan

function text_wrap_cyan() {
    if [ "$1" != '' ]; 
        then 
            printf "%s %s %s \\n" "${CYAN}" "$1" "${NORMAL}" 
        else 
            printf "%s %s %s \\n" "${CYAN}" "NoTextGiven ToBeWrapped InRed \\nUSAGE: sumu text_wrap_COLOR TEXT" "${NORMAL}" 
    fi
}

# text wrap magenta

function text_wrap_magenta() {
    if [ "$1" != '' ]; 
        then 
            printf "%s %s %s \\n" "${MAGENTA}" "$1" "${NORMAL}" 
        else 
            printf "%s %s %s \\n" "${MAGENTA}" "NoTextGiven ToBeWrapped InRed \\nUSAGE: sumu text_wrap_COLOR TEXT" "${NORMAL}" 
    fi
}

# text wrap limeyellow

function text_wrap_limeyellow() {
    if [ "$1" != '' ]; 
        then 
            printf "%s %s %s \\n" "${LIME_YELLOW}" "$1" "${NORMAL}" 
        else 
            printf "%s %s %s \\n" "${LIME_YELLOW}" "NoTextGiven ToBeWrapped InRed \\nUSAGE: sumu text_wrap_COLOR TEXT" "${NORMAL}" 
    fi
}


# text wrap powderblue

function text_wrap_powderblue() {
    if [ "$1" != '' ]; 
        then 
            printf "%s %s %s \\n" "${POWDER_BLUE}" "$1" "${NORMAL}" 
        else 
            printf "%s %s %s \\n" "${POWDER_BLUE}" "NoTextGiven ToBeWrapped InRed \\nUSAGE: sumu text_wrap_COLOR TEXT" "${NORMAL}" 
    fi
}


