#!/usr/bin/env perl

use strict;
use warnings;

our @ARGV;

if ( scalar @ARGV > '1' )  {
    #
    print _add_numbers( @ARGV );
    #
} else {
    print "Less than 2 arguments provided";
}

sub _add_numbers {
    
    #
    my @numbers = @_;

    #
    my $total;

    #
    for (@numbers) {
        #
        if ( $_ =~ /\d+/ ) { 
            $total += $_; 
        }
        #
    }

    #
    return $total;
}


1;

__DATA__
