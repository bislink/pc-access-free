#!/usr/bin/env perl

use strict;
use warnings;

our @ARGV;

if ( scalar @ARGV > '1' and scalar @ARGV <= 2 )  {
    #
    print _subtract_numbers( @ARGV );
    #
} else {
    print "Arguments: Less than 2 or more than 2 provided. ";
}

sub _subtract_numbers {
    
    #
    my @numbers = @_;

    #
    my $result;

    #
    if ( $numbers[0] =~ /\d+/ and $numbers[1] =~ /\d+/ ) {
        $result = $numbers[0] - $numbers[1];
    } else {
        $result = qq{Arguments are not numbers};
    }

    #
    return $result;
}


1;

__DATA__
