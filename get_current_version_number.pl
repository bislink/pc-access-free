#!/usr/bin/perl

use strict;
use warnings;

my $CURRENT_USER="$ENV{USER}";
    chomp $CURRENT_USER;

my $DIR = '';

if ( $CURRENT_USER eq 'root' ) { $DIR = "/usr/share/sumu"; }
elsif ( $CURRENT_USER eq 'ns21u2204' ) { $DIR = "/mnt/c/inetpub/wwwroot/pc-access-free"; }
else { $DIR = "."; }

my $current_commit_id = `$DIR/get_commit_id.pl`;
chomp $current_commit_id;

#
print convert_2_version();

=head1 NAME

    Convert NNN to N.N.N

=head2 DESC

=cut

sub convert_2_version {
    #
    my @val = ();
    #
    @val = split(//, $current_commit_id);
    #
    my $out = '';
    for (@val) {
        $out .= qq{$_.};
    }
    #
    $out =~ s!\.$!!;
    #
    return $out;
}

1;

