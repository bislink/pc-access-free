#!/usr/bin/perl

use strict;

print prune_text( text => qq{$ARGV[0]} );

sub prune_text {
	#
	my %in = (
		text => '',
		@_,
	);
	#

	if ( $in{text} ne '' ) {
		# metachars
			$in{text} =~ s!(\:|\_|\$|\@|\/|\||\*|\&|\%|\^|\!|\#|\>|\'|\"|\;|\(|\)|\+|\~)!\\$1!g;
			# deal separately and differently with new lines
			$in{text} =~ s!\n! N_L !g;
			#
			return qq{$in{text}};
			#
	} else {
		#
		return qq{23 Empty};
		#
	}
	#
}



1;
